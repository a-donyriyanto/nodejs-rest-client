const axios = require('axios');
const API_KEY = '6kNcnh-qkHUQEZ8V5lUq';
const END_POINT = 'https://marketdata.tradermade.com/api/v1/live';
let currency = ['EURUSD', 'GBPUSD' , 'UK100']; 


//Step 1: Set a global variable
let prom = []; 

//Step 2: Define axios promise
getCurrency = cur_code => axios.get(END_POINT + `?currency=${cur_code}&api_key=${API_KEY}`);

//Step 3: Push each promise to global variable
currency.forEach((cur) => {
    prom.push(getCurrency(cur))
  })

//Step 4: Use promise.all to make sure all request had already done before next process
Promise
  .all(prom)
  .then(parseResult);

//Step 5: Create function to process results
function parseResult(results) {
  console.log("All request done!")
  results.forEach(res => {
        for (quote in res.data.quotes){
          quoteData = res.data.quotes[quote];
          ccy = ""
          if ( quoteData["base_currency"] != undefined){
              ccy = quoteData["base_currency"] + quoteData["quote_currency"]          
          }else{
              ccy = quoteData["instrument"]
          }
          console.log(" Symbol " + ccy + " Bid " + quoteData["bid"] + " Ask " + quoteData["ask"])
        }
      })
  }
